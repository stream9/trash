#include "command_line.hpp"

#include "namespace.hpp"

#include <stream9/log.hpp>
#include <stream9/string_view.hpp>

#include <cassert>
#include <iostream>

#include <getopt.h>

using st9::log::dbg;

static char const* program_name = nullptr;

static struct ::option long_options[] = {
    { "list", no_argument, nullptr, 0 },
    { "trash", no_argument, nullptr, 0 },
    { "restore", no_argument, nullptr, 0 },
    { "delete", no_argument, nullptr, 0 },
    { "info", no_argument, nullptr, 'i' },
    { "directory", no_argument, nullptr, 0 },
    { "filesystem", required_argument, nullptr, 0 },
    { "empty", no_argument, nullptr, 0 },
    { "json", no_argument, nullptr, 0 },
    { "help", no_argument, nullptr, 0 },
    { "list-options", no_argument, nullptr, 0 },
    { "complete", required_argument, nullptr, 0 },
    { 0, 0, nullptr, 0 },
};

static void
print_usage()
{
    std::cout
        << "Usage: " << program_name << " [COMMAND] [OPTION]... [FILE]...\n"
        << "\n"
        << "Command:\n"
        << "      --list\t\tList trash (default if there aren't any FILE specified)\n"
        << "      --trash\t\tTrash file (default if there are FILE(s) specified)\n"
        << "      --restore\t\tRestore trash(es)\n"
        << "      --delete\t\tDelete trash(es)\n"
        << "  -i, --info\t\tDisplay trash's infomation\n"
        << "      --directory\tList trash directories for each filesystem\n"
        << "      --empty\t\tEmpty trash directory\n"
        << "\n"
        << "Filter:\n"
        << "      --filesystem\tFilter trash by filesystem which path belong to\n"
        << "\n"
        << "Sort:\n"
        << "      -t\tSort by time, newest first (default)\n"
        << "      -n\tSort by name\n"
        << "      -S\tSort by size, largest first\n"
        << "\n"
        << "      -r\tReverse sort order\n"
        << "\n"
        << "Misc:\n"
        << "      --json\tJSON output mode\n"
        << "      --verbose\tPrint detailed message\n"
        << "      --help\tPrint this message\n"
        ;
}

static void
list_options(program_option const&)
{
    std::cout << "--list "
              << "--trash "
              << "--restore "
              << "--delete "
              << "--info "
              << "--directory "
              << "--filesystem "
              << "--empty "
              << "--json "
              << "--verbose "
              << "--help "
              << "-i -t -n -S -r "
              << std::endl;
}

static void
print_error(st9::string_view message = "")
{
    if (!message.empty()) {
        std::cerr << message << std::endl;
    }
    std::cerr << "Try '" << program_name << " --help' for more information." << std::endl;
}

static void
set_command(program_option& opt, command const cmd)
{
    if (opt.command == command::none) {
        opt.command = cmd;
    }
    else {
        print_error("You can't specify multiple command.");
        std::exit(1);
    }
}

static void
process_long_option(program_option& opt, int const index)
{
    switch (index) {
        case 0:
            set_command(opt, command::list);
            break;
        case 1:
            set_command(opt, command::trash);
            break;
        case 2:
            set_command(opt, command::restore);
            break;
        case 3:
            set_command(opt, command::remove);
            break;
        case 4:
            set_command(opt, command::info);
            break;
        case 5:
            set_command(opt, command::directory);
            break;
        case 6:
            opt.filesystem_path = optarg;
            break;
        case 7:
            set_command(opt, command::empty);
            break;
        case 8:
            opt.json = true;
            break;
        case 9:
            print_usage();
            std::exit(0);
            break;
        case 10:
            opt.complete_option = true;
            break;
        case 11:
            opt.complete_arg = true;
            opt.complete_key = optarg;
            break;
        default:
            dbg() << "unknown long option:" << index;
            std::abort();
    }
}

static void
process_short_option(program_option& opt, int const key)
{
    switch (key) {
        case 'i':
            set_command(opt, command::info);
            break;
        case 'v':
            opt.verbose = true;
            break;
        case 'n':
            opt.sort_by = sort_criteria::name;
            break;
        case 't':
            opt.sort_by = sort_criteria::time;
            break;
        case 'S':
            opt.sort_by = sort_criteria::size;
            break;
        case 'r':
            opt.reverse_sort = true;
            break;
        default:
            dbg() << "unknown short option:" << key;
            std::abort();
    }
}

static void
process_option(program_option& opt, int argc, char* argv[])
{
    while (true) {
        int option_index = 0;
        auto const key =
            ::getopt_long(argc, argv, "ivntSr", long_options, &option_index);

        switch (key) {
            default:
                process_short_option(opt, key);
                break;
            case 0:
                process_long_option(opt, option_index);
                break;
            case -1:
                return;
            case '?':
                print_error();
                std::exit(1);
                break;
        }
    }
}

static void
set_default_option(program_option& opt)
{
    if (opt.complete_arg || opt.complete_option) return;

    if (opt.command == command::none) {
        if (opt.args.empty()) {
            opt.command = command::list;
        }
        else {
            opt.command = command::trash;
        }
    }

    if (opt.sort_by == sort_criteria::none) {
        opt.sort_by = sort_criteria::time;
    }
}

program_option
parse_arg(int argc, char* argv[])
{
    program_name = argv[0];
    program_option opt;

    process_option(opt, argc, argv);

    for (auto i = optind; i < argc; ++i) {
        opt.args.insert(argv[i]);
    }

    set_default_option(opt);

    if (opt.complete_option) {
        list_options(opt);
        std::exit(0);
    }

    return opt;
}
