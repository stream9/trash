#include "entry_iterator_set.hpp"

#include <fnmatch.h>

#include <stream9/sort.hpp>
#include <stream9/strings/lexicographical_compare.hpp>

static bool
glob_match(str::cstring_view const pattern, str::cstring_view const name)
{
    return ::fnmatch(pattern, name, 0) == 0;
}

/*
 * class entry_iterator_set
 */
entry_iterator_set::
entry_iterator_set(trash::trash_can const& can, //TODO error
          st9::array<char const*> const& globs)
{
    if (globs.empty()) {
        for (auto it = can.begin(); it != can.end(); ++it) {
            m_entries.insert(it);
        }
    }
    else {
        for (auto const& glob: globs) {
            bool found = false;

            for (auto it = can.begin(); it != can.end(); ++it) {
                if (glob_match(glob, it->name())) {
                    found = true;
                    m_entries.insert(it);
                }
            }

            if (!found) {
                std::cerr << "trash doesn't exist: " << glob << std::endl;
            }
        }
    }
}

entry_iterator_set::const_iterator entry_iterator_set::
begin() const
{
    return m_entries.begin();
}

entry_iterator_set::const_iterator entry_iterator_set::
end() const
{
    return m_entries.end();
}

void entry_iterator_set::
sort_by_name(bool const reverse/*= false*/)
{
    st9::sort(m_entries, [&](auto& i1, auto& i2) {
        if (reverse) {
            return str::ilexicographical_compare(i2->name(), i1->name());
        }
        else {
            return str::ilexicographical_compare(i1->name(), i2->name());
        }
    });
}

void entry_iterator_set::
push_back(base_iterator const it)
{
    m_entries.insert(it);
}

entry_iterator_set::const_iterator entry_iterator_set::
erase(const_iterator const it)
{
    return m_entries.erase(it);
}

void entry_iterator_set::
sort_by_time(bool const reverse/*= false*/)
{
    st9::sort(m_entries, [&](auto& i1, auto& i2) {
        auto const& t1 = i1->trash_info().deletion_date();
        auto const& t2 = i2->trash_info().deletion_date();

        if (reverse) {
            return t1 < t2;
        }
        else {
            return t1 > t2;
        }
    });
}

void entry_iterator_set::
sort_by_size(bool const reverse/*= false*/)
{
    st9::sort(m_entries, [&](auto& i1, auto& i2) {
        auto const& s1 = i1->file_size();
        auto const& s2 = i2->file_size();

        if (reverse) {
            return s1 < s2;
        }
        else {
            return s1 > s2;
        }
    });
}

