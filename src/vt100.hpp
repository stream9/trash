#ifndef TRASH_VT100_HPP
#define TRASH_VT100_HPP

#include <stream9/string.hpp>

#include <ostream>

namespace vt100 {

class control_sequence //TODO constexpr when gcc support constexpr string
{
public:
    template<std::convertible_to<st9::string_view> T>
    control_sequence(T const& code) : m_code { code } {}

    control_sequence(st9::string code)
        : m_code { std::move(code) }
    {}

    control_sequence operator+(control_sequence const& o) const
    {
        auto code = m_code;
        code.insert(';');
        code.append(o.m_code);

        return code;
    }

    friend std::ostream& operator<<(std::ostream& os, control_sequence const& a)
    {
        return os << "\x1b[" << a.m_code << 'm';
    }

private:
    st9::string m_code;
};

control_sequence bright() { return "1"; }
control_sequence dim() { return "2"; }
control_sequence underline() { return "4"; }
control_sequence blink() { return "5"; }
control_sequence reverse() { return "7"; }
control_sequence hidden() { return "8"; }

control_sequence reset_all() { return "0"; }
control_sequence reset_bright() { return "21"; }
control_sequence reset_underline() { return "24"; }
control_sequence reset_blink() { return "25"; }
control_sequence reset_reverse() { return "27"; }
control_sequence reset_hidden() { return "28"; }

/*
 * foreground (8 / 16 colors)
 */
namespace foreground {

    control_sequence default_color() { return "39"; }
    control_sequence black() { return "30"; }
    control_sequence red() { return "31"; }
    control_sequence green() { return "32"; }
    control_sequence yellow() { return "33"; }
    control_sequence blue() { return "34"; }
    control_sequence magenta() { return "35"; }
    control_sequence cyan() { return "36"; }
    control_sequence light_gray() { return "37"; }
    control_sequence dark_gray() { return "90"; }
    control_sequence dark_red() { return "91"; }
    control_sequence dark_green() { return "92"; }
    control_sequence dark_yellow() { return "93"; }
    control_sequence dark_blue() { return "94"; }
    control_sequence dark_magenta() { return "95"; }
    control_sequence dark_cyan() { return "96"; }
    control_sequence white() { return "97"; }

} // namespace foreground

/*
 * background (8 / 16 colors)
 */
namespace background {

    control_sequence default_color() { return "49"; }
    control_sequence black() { return "40"; }
    control_sequence red() { return "41"; }
    control_sequence green() { return "42"; }
    control_sequence yellow() { return "43"; }
    control_sequence blue() { return "44"; }
    control_sequence magenta() { return "45"; }
    control_sequence cyan() { return "46"; }
    control_sequence light_gray() { return "47"; }
    control_sequence dark_gray() { return "100"; }
    control_sequence dark_red() { return "101"; }
    control_sequence dark_green() { return "102"; }
    control_sequence dark_yellow() { return "103"; }
    control_sequence dark_blue() { return "104"; }
    control_sequence dark_magenta() { return "105"; }
    control_sequence dark_cyan() { return "106"; }
    control_sequence white() { return "107"; }

} // namespace background

control_sequence
foreground256(int code)
{
    return "38;5;" + std::to_string(code);
}

control_sequence
background256(int code)
{
    return "48;5;" + std::to_string(code);
}

} // namespace vt100

#endif // TRASH_VT100_HPP
