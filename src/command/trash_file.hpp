#ifndef TRASH_TRASH_FILE_HPP
#define TRASH_TRASH_FILE_HPP

#include "../command_line.hpp"
#include "../namespace.hpp"

#include <stream9/xdg/trash.hpp>

bool trash_file(trash::trash_can&, program_option const&);

#endif // TRASH_TRASH_FILE_HPP
