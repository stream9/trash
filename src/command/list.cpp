#include "list.hpp"

#include "../entry_iterator_set.hpp"
#include "../format.hpp"
#include "../namespace.hpp"
#include "../vt100.hpp"

#include <stream9/any_of.hpp>
#include <stream9/log.hpp>
#include <stream9/string.hpp>
#include <stream9/unique_table.hpp>

#include <iomanip>
#include <ostream>

#include <fnmatch.h>

static void
print_line(std::ostream& os, auto const& entry)
{
    auto const& name = entry.name();
    auto const& info = entry.trash_info();

    os << format_time(info.deletion_date())
       << " "
       << std::setw(4) << std::right << format_size(entry.file_size())
                       << std::left
       << " "
       << format_name(name, info)
       << std::endl;
}

static bool
match_globs(trash::trash_can::const_iterator const it,
            auto const& globs)
{
    if (globs.empty()) {
        return true;
    }
    else {
        return st9::any_of(globs, [&](auto&& g) {
            return ::fnmatch(g, it->name(), 0) == 0;
        });
    }
}

static auto
group_entries_by_filesystem(entry_iterator_set const& entries,
                            trash::top_directory_set const& top_dirs)
{
    using result_t = st9::unique_table<st9::string, entry_iterator_set>;

    result_t result;

    for (auto it = entries.begin(); it != entries.end(); ++it) {
        auto const& trash_dir = (*it)->trash_directory();
        auto const top_it = top_dirs.find(trash_dir);

        st9::string top_dir;
        if (top_it != top_dirs.end()) {
            top_dir = top_it->path();
        }

        // empty top_dir mean entry is in home trash directory
        result[top_dir].push_back(*it);
    }

    return result;
}

static auto
sum_file_size(entry_iterator_set const& entries)
{
    trash::file_size_t result = 0;

    for (auto const& it: entries) {
        result += it->file_size();
    }

    return result;
}

static void
filter_entries_by_filesystem(entry_iterator_set& entries,
                             trash::top_directory_set const& top_dirs,
                             st9::string_view const filter_path)
{
    if (filter_path.empty()) return;

    auto const filter_top_it = top_dirs.find(filter_path);

    for (auto it = entries.begin(); it != entries.end();) {
        auto const& trash_dir = (*it)->trash_directory();

        auto const top_it = top_dirs.find(trash_dir.path());
        if (top_it != filter_top_it) {
            it = entries.erase(it);
        }
        else {
            ++it;
        }
    }
}

void
list(trash::trash_can const& can, program_option const& opt)
{
    entry_iterator_set entries { can, opt.args };

    auto const& top_dirs = can.top_directories();

    filter_entries_by_filesystem(entries, top_dirs, opt.filesystem_path);

    for (auto&& [top_dir, entries]: group_entries_by_filesystem(entries, top_dirs)) {
        if (top_dir.empty()) {
            std::cout << vt100::underline()
                      << "Home trash directory"
                      << vt100::reset_underline() << ": "
                      << format_size(sum_file_size(entries)) << "B used"
                      << std::endl;
        }
        else {
            std::cout << std::endl
                      << vt100::underline()
                      << "Filesystem at " << top_dir
                      << vt100::reset_underline() << ": "
                      << format_size(sum_file_size(entries)) << "B used"
                      << std::endl;
        }

        switch (opt.sort_by) {
            using S = sort_criteria;
            case S::time:
                entries.sort_by_time(opt.reverse_sort);
                break;
            case S::size:
                entries.sort_by_size(opt.reverse_sort);
                break;
            default:
                entries.sort_by_name(opt.reverse_sort);
                break;
        }

        for (auto const& it: entries) {
            print_line(std::cout, *it);
        }
    }
}
