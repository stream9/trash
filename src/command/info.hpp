#ifndef TRASH_INFO_HPP
#define TRASH_INFO_HPP

#include "../command_line.hpp"
#include "../namespace.hpp"

#include <stream9/xdg/trash.hpp>

void info(trash::trash_can&, program_option const&);

#endif // TRASH_INFO_HPP
