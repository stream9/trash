#ifndef TRASH_COMMAND_RESTORE_FILE_HPP
#define TRASH_COMMAND_RESTORE_FILE_HPP

#include "../command_line.hpp"
#include "../namespace.hpp"

#include <stream9/xdg/trash.hpp>

void restore_file(trash::trash_can&, program_option const&);

#endif // TRASH_COMMAND_RESTORE_FILE_HPP
