#include "restore_file.hpp"

#include "../command_line.hpp"
#include "../entry_iterator_set.hpp"
#include "../namespace.hpp"

#include <iostream>

#include <stream9/errors.hpp>
#include <stream9/xdg/trash.hpp>

void
remove_file(trash::trash_can& can, program_option const& opt)
{
    entry_iterator_set entries { can, opt.args };

    for (auto const& it: entries) {
        try {
            can.erase_file(it);
        }
        catch (...) {
            std::cerr << "fail to remove trash: " << it->name() << std::endl;
            if (opt.verbose) {
                st9::print_error(std::cerr);
            }
        }
    }
}
