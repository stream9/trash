#ifndef TRASH_LIST_HPP
#define TRASH_LIST_HPP

#include "../command_line.hpp"
#include "../namespace.hpp"

#include <stream9/xdg/trash.hpp>

void list(trash::trash_can const&, program_option const&);

#endif // TRASH_LIST_HPP
