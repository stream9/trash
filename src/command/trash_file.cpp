#include "../command_line.hpp"
#include "../namespace.hpp"

#include <stream9/errors.hpp>
#include <stream9/string_view.hpp>
#include <stream9/xdg/trash.hpp>

static void
print_error(st9::string_view msg,
            st9::string_view filename,
            program_option const& opt)
{
    if (opt.json) {
        std::cerr << json::format({
            { "type", "error" },
            { "message", msg },
            { "filename", filename },
        }) << std::endl;
    }
    else {
        std::cerr << msg << ": " << filename << std::endl;
    }
}

bool
trash_file(trash::trash_can& can, program_option const& opt)
{
    assert(!opt.args.empty());

    bool result = true;

    for (auto const& filename: opt.args) {
        try {
            can.trash_file(filename);
        }
        catch (...) {
            try {
                throw;
            }
            catch (st9::error const& e) {
                if (e.why() == trash::errc::file_does_not_exist) {
                    print_error("file does not exist", filename, opt);
                }
                else {
                    print_error(e.why().message(), filename, opt);
                }
            }
            catch (...) {
                print_error("fail to trash file", filename, opt);
            }

            if (opt.verbose) {
                st9::print_error(std::cerr);
            }

            result = false;
        }
    }

    return result;
}
