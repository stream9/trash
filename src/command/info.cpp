#include "info.hpp"

#include "../entry_iterator_set.hpp"
#include "../format.hpp"
#include "../namespace.hpp"

#include <stream9/errors.hpp>

#include <iostream>

static void
print_entry(trash::trash_entry const& entry, bool const verbose)
{
    try {
        auto const& info = entry.trash_info();

        std::cout << "Name: " << entry.name() << std::endl;
        std::cout << "Size: " << format_size(entry.file_size()) << "B" << std::endl;
        std::cout << "Modified Date: "
                  << format_time(entry.last_modified_time()) << std::endl;
        std::cout << "Deletion Date: "
                  << format_time(entry.deletion_time()) << std::endl;
        std::cout << "Original Path: " << tildify(info.original_path())
                  << std::endl;
    }
    catch (...) {
        std::cerr << "fail to retrieve trash informatin: "
                  << entry.name() << std::endl;
        if (verbose) {
            st9::print_error(std::cerr);
        }
    }
}

void
info(trash::trash_can& can, program_option const& opt)
{
    entry_iterator_set entries { can, opt.args };

    switch (opt.sort_by) {
        using S = sort_criteria;
        case S::time:
            entries.sort_by_time(opt.reverse_sort);
            break;
        case S::size:
            entries.sort_by_size(opt.reverse_sort);
            break;
        default:
            entries.sort_by_name(opt.reverse_sort);
            break;
    }

    bool first = true;
    for (auto const& it: entries) {
        if (first) {
            first = false;
        }
        else {
            std::cout << std::endl;
        }

        print_entry(*it, opt.verbose);
    }
}
