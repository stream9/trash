#include "empty.hpp"

#include "../command_line.hpp"
#include "../namespace.hpp"

#include <stream9/xdg/trash.hpp>

void
empty(trash::trash_can& can, program_option const&)
{
    can.clear();
}
