#ifndef TRASH_DIRECTORY_HPP
#define TRASH_DIRECTORY_HPP

#include "../command_line.hpp"
#include "../namespace.hpp"

#include <stream9/xdg/trash.hpp>

void directory(trash::trash_can const&, program_option const&);

#endif // TRASH_DIRECTORY_HPP
