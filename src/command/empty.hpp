#ifndef TRASH_COMMAND_EMPTY_HPP
#define TRASH_COMMAND_EMPTY_HPP

#include "../command_line.hpp"
#include "../namespace.hpp"

#include <stream9/xdg/trash.hpp>

void empty(trash::trash_can&, program_option const&);

#endif // TRASH_COMMAND_EMPTY_HPP
