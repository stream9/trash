#include "restore_file.hpp"

#include "../command_line.hpp"
#include "../entry_iterator_set.hpp"
#include "../format.hpp"
#include "../namespace.hpp"

#include <iostream>

#include <stream9/errors.hpp>
#include <stream9/json.hpp>
#include <stream9/xdg/trash.hpp>

void
restore_file(trash::trash_can& can, program_option const& opt)
{
    entry_iterator_set entries { can, opt.args };

    for (auto const& it: entries) {
        try {
            can.restore_file(it);
        }
        catch (...) {
            try {
                throw;
            }
            catch (st9::error const& e) {
                if (e.why() == trash::errc::file_already_exist) {
                    auto& cxt = e.context();
                    auto const& o_dest = json::find_string(cxt, "destination");

                    if (o_dest) {
                        std::cerr << "destination path is already taken: "
                                  << tildify(o_dest->c_str()) << std::endl;
                    }
                }
            }
            catch (...) {
                std::cerr << "fail to restore trash: " << it->name() << std::endl;
            }

            if (opt.verbose) {
                st9::print_error(std::cerr);
            }
        }
    }
}
