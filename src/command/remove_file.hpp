#ifndef TRASH_COMMAND_REMOVE_FILE_HPP
#define TRASH_COMMAND_REMOVE_FILE_HPP

#include "../command_line.hpp"
#include "../namespace.hpp"

#include <stream9/xdg/trash.hpp>

void remove_file(trash::trash_can&, program_option const&);

#endif // TRASH_COMMAND_REMOVE_FILE_HPP

