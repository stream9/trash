#include "directory.hpp"

#include "../command_line.hpp"
#include "../format.hpp"
#include "../namespace.hpp"

#include <cstdint>
#include <iostream>

#include <stream9/cstring_ptr.hpp>
#include <stream9/errors.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/path/absolute.hpp>
#include <stream9/path/home_directory.hpp>
#include <stream9/path/normalize.hpp>
#include <stream9/string.hpp>
#include <stream9/xdg/trash.hpp>

#include <sys/statvfs.h> //TODO factor out

static std::uintmax_t
available_size(st9::cstring_ptr p)
{
    struct ::statvfs buf {};
    auto rc = ::statvfs(p, &buf);
    if (rc == -1) {
        throw st9::error {
            "statvfs(3)",
            lx::make_error_code(errno)
        };
    }

    return buf.f_bavail * buf.f_bsize;
}

static st9::string
make_indent(int n)
{
    st9::string s;

    s.reserve(n);
    for (auto i = 0; i < n; ++i) {
        s.insert(' ');
    }

    return s;
}

static void
print_trash_directory(trash::trash_directory const& d, int const indent = 0)
{
    auto i = make_indent(indent);

    std::cout << i << "Location: " << tildify(d.path()) << std::endl;
    std::cout << i << "Used: " << format_size(d.directory_size()) << "B / "
                   << format_size(d.directory_size_limit()) << "B" << std::endl;
}

static void
print_home_trash_directory(trash::trash_can const& can, program_option const& opt)
{
    try {
        auto const& home = can.home_trash_directory();

        std::cout << "Home trash directory" << std::endl;
        print_trash_directory(home);
        std::cout << "Avail: " << format_size(available_size(home.path()))
                  << "B" << std::endl;
    }
    catch (...) {
        std::cerr << "fail to get information of trash directory" << std::endl;

        if (opt.verbose) {
            st9::print_error(std::cerr);
        }
    }
}

static void
print_filesystem(trash::top_directory const& top_dir, program_option const& opt)
{
    try {
        std::cout << "Mount point: " << tildify(top_dir.path()) << std::endl;
        std::cout << "Avail: " << format_size(
                       available_size(top_dir.path())) << "B" << std::endl;

        int trash_no = 1;
        for (auto const& trash_dir: top_dir) {
            std::cout << "  Trash directory #" << trash_no << std::endl;
            print_trash_directory(trash_dir, 2);
            ++trash_no;
        }
    }
    catch (...) {
        std::cerr << "fail to get information of trash directory" << std::endl;

        if (opt.verbose) {
            st9::print_error(std::cerr);
        }
    }
}

void
directory(trash::trash_can const& can, program_option const& opt)
{
    try {
        if (opt.args.empty()) {
            print_home_trash_directory(can, opt);

            int top_no = 1;
            for (auto const& top_dir: can.top_directories()) {
                if (top_dir.empty()) continue;

                std::cout << std::endl;
                std::cout << "Filesystem #" << top_no << std::endl;
                print_filesystem(top_dir, opt);

                ++top_no;
            }
        }
        else {
            auto path = path::normalize(path::to_absolute(opt.args.front()));
            auto const& top_dirs = can.top_directories();

            auto const& it1 = top_dirs.find(path::home_directory());
            if (it1 == top_dirs.end()) {
                std::cerr << "fail to find top directory of filesystem "
                             "which contains home directory" << std::endl;
                return;
            }

            auto const& it2 = top_dirs.find(path);
            if (it2 == top_dirs.end()) {
                std::cerr << "fail to find top directory of filesystem "
                             "which contains " << path << std::endl;
                return;
            }

            bool need_empty_line = false;
            if (*it1 == *it2) {
                print_home_trash_directory(can, opt);
                need_empty_line = true;
            }

            if (!it2->empty()) {
                if (need_empty_line) {
                    std::cout << std::endl;
                }

                std::cout << "Filesystem" << std::endl;
                print_filesystem(*it2, opt);
            }
        }
    }
    catch (...) {
        std::cerr << "fail to get information of trash directory" << std::endl;

        if (opt.verbose) {
            st9::print_error(std::cerr);
        }
    }
}
