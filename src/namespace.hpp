#ifndef TRASH_NAMESPACE_HPP
#define TRASH_NAMESPACE_HPP

namespace stream9::filesystem {}
namespace stream9::iterators {}
namespace stream9::json {}
namespace stream9::linux {}
namespace stream9::path {}
namespace stream9::ranges {}
namespace stream9::strings {}
namespace stream9::xdg::trash {}

namespace st9 = stream9;
namespace fs = st9::filesystem;
namespace iter = st9::iterators;
namespace json = st9::json;
namespace lx = st9::linux;
namespace path = st9::path;
namespace rng = st9::ranges;
namespace str = st9::strings;
namespace trash = st9::xdg::trash;

#endif // TRASH_NAMESPACE_HPP
