#include "command/directory.hpp"
#include "command/empty.hpp"
#include "command/info.hpp"
#include "command/list.hpp"
#include "command/remove_file.hpp"
#include "command/restore_file.hpp"
#include "command/trash_file.hpp"
#include "command_line.hpp"
#include "complete.hpp"
#include "entry_iterator_set.hpp"
#include "format.hpp"
#include "namespace.hpp"

#include <iostream>

#include <stream9/errors.hpp>
#include <stream9/filesystem.hpp>
#include <stream9/json.hpp>
#include <stream9/shared_node.hpp>
#include <stream9/xdg/trash.hpp>

//TODO JSON output mode
//TODO awesome curses interface?
//TODO turn this into dbus service?
//TODO fuse
//TODO delay scanning of directories until it becomes necessary
//TODO combine --list and --info
//TODO display time like "1 hour ago"
//TODO context dependent option completion
//TODO specify restore destination from command line
//TODO --empty to show progress
//TODO factor out non-portable code into system class.
//TODO namespace app
int main(int argc, char* argv[])
{
    try {
        auto const& opt = parse_arg(argc, argv);

        st9::shared_node<trash::linux_environment> env;
        auto can = trash::trash_can::construct(env);

        if (opt.complete_arg) {
            return complete(*can, opt);
        }
        else {
            bool success = true;
            switch (opt.command) {
                case command::list:
                    list(*can, opt);
                    break;
                case command::trash:
                    success = trash_file(*can, opt);
                    break;
                case command::restore:
                    restore_file(*can, opt);
                    break;
                case command::remove:
                    remove_file(*can, opt);
                    break;
                case command::info:
                    info(*can, opt);
                    break;
                case command::directory:
                    directory(*can, opt);
                    break;
                case command::empty:
                    empty(*can, opt);
                    break;
                case command::none:
                    assert(false);
                    break;
            }

            return success ? 0 : 1;
        }
    }
    catch (...) {
        st9::print_error(std::cerr);
        return 1;
    }
}
