#include "complete.hpp"

#include <iostream>

#include <stream9/strings/starts_with.hpp>
#include <stream9/string_view.hpp>

static void
complete_trash_name(trash::trash_can const& can, st9::string_view prefix)
{
    for (auto const& ent: can) {
        if (!prefix.empty()) {
            if (str::istarts_with(ent.name(), prefix)) {
                std::cout << ent.name() << std::endl;
            }
        }
        else {
            std::cout << ent.name() << std::endl;
        }
    }
}

int
complete(trash::trash_can const& can, program_option const& opt)
{
    if (opt.command == command::restore ||
        opt.command == command::remove ||
        opt.command == command::list ||
        opt.command == command::info)
    {
        complete_trash_name(can, opt.complete_key);
        return 0;
    }
    else {
        return 1;
    }
}
