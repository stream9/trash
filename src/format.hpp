#ifndef TRASH_FORMAT_HPP
#define TRASH_FORMAT_HPP

#include <chrono>
#include <cstddef>
#include <iomanip>
#include <span>

#include <stream9/filesystem/is_directory.hpp>
#include <stream9/path/home_directory.hpp>
#include <stream9/string.hpp>
#include <stream9/strings/stream.hpp>
#include <stream9/strings/view/find_replace.hpp>
#include <stream9/to_string.hpp>

template<typename T = std::uint64_t, int precision = -1>
st9::string
format_size(T v,
            std::span<char const*> units_ = {},
            T unit_base = T(1024) ) // move to stream9::strings
{
    using str::operator<<;

    static char const* default_units[] = { "", "K", "M", "G", "T", "P", "E", "Z" };

    std::span<char const*> units;
    if (units_.empty()) {
        units = default_units;
    }
    else {
        units = units_;
    }

    std::size_t i = 0;
    auto dv = static_cast<double>(v);
    for (; i < units.size(); ++i) {
        if (dv < static_cast<double>(unit_base)) {
            break;
        }
        else {
            dv /= static_cast<double>(unit_base);
        }
    }

    st9::string s;
    if constexpr (precision != -1) {
        s << std::setprecision(precision) << dv;
    }
    else {
        if (dv < 10.0) {
            s << std::setprecision(2) << dv;
        }
        else {
            s << std::round(dv);
        }
    }

    if (st9::string_view const u = units[i]; !u.empty()) {
        s.append(u);
    }

    return s;
}

inline st9::string
format_name(st9::string_view const name, auto const& info)
{
    st9::string result { name };

    auto const& file_path = info.file_path();
    if (fs::is_directory(file_path)) { //TODO
        result.insert('/');
    }

    return result;
}

template<typename Clock, typename Duration>
st9::string
format_time(std::chrono::time_point<Clock, Duration> const tp)
{
    using namespace std::literals;

    st9::string result;

    if (tp < Clock::now() - std::chrono::years(1)) {
        result = st9::to_string(tp, "%b %e %Y");
    }
    else {
        result = st9::to_string(tp, "%b %e %H:%M");
    }

    return result;
}

inline st9::string
tildify(st9::cstring_ptr path) //TODO move to stream9::filesystem
{
    auto const& home = path::home_directory();

    return str::views::find_replace(path.c_str(), home.c_str(), "~");
}

#endif // TRASH_FORMAT_HPP
