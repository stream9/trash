#/usr/bin/env bash
_trash_completion()
{
    local current=$2

    if [[ "${current}" =~ -.* ]]
    then
        # remove $current from command line so argp won't confuse it as option.
        local cmdline=( ${COMP_WORDS[@]:0:${COMP_CWORD}} )
        cmdline+=("--list-options")

        COMPREPLY=( $(compgen -W "$(${cmdline[@]})" -- "${current}") )
    else
        # unescape current
        local unescaped
        read <<< "${current}"; unescaped=$REPLY

        local completion
        completion=$(${COMP_WORDS[@]} --complete="${unescaped}")
        if [[ $? == 0 ]]
        then
            local IFS=$'\n'
            COMPREPLY=( $(compgen -W "${completion}") )
        else
            local IFS=$'\n'
            COMPREPLY=( $(compgen -f -- "${unescaped}") )
        fi
    fi
}

complete -F _trash_completion -o filenames trash
