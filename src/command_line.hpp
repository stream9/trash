#ifndef STREAM9_TRASH_CLI_COMMAND_LINE_HPP
#define STREAM9_TRASH_CLI_COMMAND_LINE_HPP

#include "namespace.hpp"

#include <stream9/array.hpp>
#include <stream9/string_view.hpp>

enum class command : unsigned {
    none,
    list,
    trash,
    restore,
    remove,
    info,
    directory,
    empty,
};

enum class sort_criteria : unsigned {
    none, name, time, size,
};

struct program_option {
    st9::array<char const*> args;
    char const* complete_key = nullptr;
    st9::string_view filesystem_path = "";
    enum command command : 3 = command::none;
    sort_criteria sort_by : 2 = sort_criteria::none;
    bool reverse_sort : 1 = false;
    bool complete_arg : 1 = false;
    bool complete_option : 1 = false;
    bool verbose : 1 = false;
    bool json : 1 = false;
};

program_option
    parse_arg(int argc, char* argv[]);

#endif // STREAM9_TRASH_CLI_COMMAND_LINE_HPP
