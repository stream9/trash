#ifndef TRASH_ENTRY_ITERATOR_SET_HPP
#define TRASH_ENTRY_ITERATOR_SET_HPP

#include "namespace.hpp"

#include <stream9/array.hpp>
#include <stream9/xdg/trash.hpp>

class entry_iterator_set
{
public:
    using base_iterator = trash::trash_can::const_iterator;
    using set_type = st9::array<base_iterator>;
    using const_iterator = rng::iterator_t<set_type const>;

public:
    // essentials
    entry_iterator_set() = default;

    entry_iterator_set(trash::trash_can const&,
                       st9::array<char const*> const& globs);

    // accesor
    const_iterator begin() const;
    const_iterator end() const;

    // query
    auto size() const { return m_entries.size(); }

    // modifier
    void push_back(base_iterator);

    const_iterator erase(const_iterator);

    void sort_by_name(bool reverse = false);
    void sort_by_time(bool reverse = false);
    void sort_by_size(bool reverse = false);

private:
    set_type m_entries;
};

#endif // TRASH_ENTRY_ITERATOR_SET_HPP
