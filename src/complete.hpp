#ifndef TRASH_COMPLETE_HPP
#define TRASH_COMPLETE_HPP

#include "command_line.hpp"
#include "namespace.hpp"

#include <stream9/xdg/trash.hpp>

int complete(trash::trash_can const&, program_option const&);

#endif // TRASH_COMPLETE_HPP
